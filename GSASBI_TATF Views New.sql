/*
  GSASBI Views - Aurora TATF data + related fields 
  REVISED   
  June 2017   bjf 
*/

drop materialized view INST_DATA.GSASBI_TATF_COURSE

CREATE MATERIALIZED VIEW INST_DATA.GSASBI_TATF_COURSE
build immediate
refresh on demand
AS
SELECT         a.empl_id ,
                    j.pri_last_name || ', ' || j.pri_first_name AS name ,
                    pd.dept_abbr as stu_home_dept_abbr ,
                    pd.dept_short as stu_home_dept_desc ,
                    pd.acad_prog as stu_acad_prog,
                    pd.student_gyear as stu_gyear,
                    pd.generals_met as stu_generals_met,
                    pd.progress as stu_progress,
                    c.course_id ,
                    c.title AS crs_title ,
                    c.subject as crs_subject ,
                    c.catalog_nbr AS crs_catalog_num ,
                    NVL (a.acad_year, c.sis_acad_year) AS crs_acad_yr ,
                    NVL (a.term_cd ,
                         CASE
                            WHEN c.sis_term_id = '8' THEN 'FALL'
                            WHEN c.sis_term_id = '2' THEN 'SPRING'
                            ELSE TO_CHAR (c.sis_term_id)
                         END) AS crs_term ,
                    c.strm as crs_strm,
                    a.appt_id , 
                    a.appt_start_date ,
                    a.planned_appt_end_date ,
                    a.assignment_eff_date ,
                    a.assignment_type ,
                    a.role_type as appt_role_type ,
                    a.role_fte as appt_role_fte ,
                    a.section_count as appt_section_cnt,
                    a.status as appt_status ,
                    j.dept_desc ,
                    j.discipline_desc ,
                    j.job_cd ,
                    j.job_desc
         FROM  AURORA_RPT.ASSG_CURR_MV A
                    JOIN HDWDATA.OHR_JOB_ACTN_ALL J ON ( j.empl_id = a.empl_id AND j.empl_rcd = a.empl_rcd AND j.eff_date = a.appt_start_date AND j.eff_seq = a.start_job_eff_date_eff_seq)
                    LEFT OUTER JOIN SIS_DATA.PROGRESS_STU_UNQ PD   ON pd.hu_id = a.empl_id
                    LEFT OUTER JOIN SIS_DATA.AUR_COURSE_DATA C  ON c.cpk = a.cpk_id ; 
                    
GRANT SELECT ON INST_DATA.GSASBI_TATF_COURSE to GSASBI ; 


