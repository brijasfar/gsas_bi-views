/*
GSAS BI VIEWS
Original version
GSASBI_AUR_TATF_COURSE_ASSIGN

by Dave Faux
priot to June 2017 
*/

CREATE OR REPLACE VIEW INST_DATA.GSASBI_AUR_TATF_APPT
AS
     SELECT j.empl_id,
            j.empl_rcd,
            a.start_job_eff_date appt_start_date,
            a.end_job_eff_date appt_end_date,
            a.expected_end_date_job_eff_date planned_end_date,
            ag.code AS aurora_group,
            j.dept_id,
            j.dept_desc,
            j.discipline_cd,
            j.discipline_desc,
            j.job_cd,
            j.job_desc,
            j.empl_class_cd,
            j.empl_class_desc,
            j.acad_rank_cd,
            j.acad_rank_desc,
            j.union_cd,
            j.union_desc,
            j.business_title,
            j.fte,
            j.std_hours,
            j.sal_admin_plan_cd,
            j.paygroup_cd
       FROM aurora_data.appointment a
            JOIN hdwdata.ohr_job_actn_all j
               ON     a.empl_id = j.empl_id
                  AND a.empl_rcd = j.empl_rcd
                  AND a.latest_effective_eff_date = j.eff_date
                  AND a.latest_effective_eff_seq = j.eff_seq
            JOIN AURORA_DATA.appt_aurora_group_x agx
               ON a.id = agx.appointment_id
            JOIN AURORA_DATA.aurora_group ag
               ON ag.id = agx.aurora_group_id
      WHERE 1 = 1 AND j.job_cd IN ('000902', '018102')
   ORDER BY a.empl_id, j.eff_date  ;


GRANT SELECT ON INST_DATA.GSASBI_AUR_TATF_APPT TO GSASBI ;


CREATE OR REPLACE VIEW INST_DATA.GSASBI_AUR_TATF_COURSE_ASSIGN
AS
     SELECT NAME,
            EMPL_ID,
            DEPT_GYEAR,
            STUDENT_GYEAR,
            HOME_DEPT_ABBR,
            HOME_DEPT_SHORT,
            JOB_DESC,
            DEPARTMENT,
            APPT_START_DATE,
            PLANNED_APPT_END_DATE,
            SUBJECT,
            CATALOG_NUMBER,
            COURSE_TITLE,
            ACADEMIC_YEAR,
            TERM_CD,
            ASSIGNMENT_EFF_DATE,
            ASSIGNMENT_TYPE,
            ROLE_TYPE,
            ROLE_FTE,
            SECTION_COUNT,
            STATUS,
            AURORA_GROUP,
            JOB_CD,
            DEPT_ID,
            DEPT_DESC,
            HVRD_EMAIL_ADDR
       FROM (SELECT j.pri_last_name || ', ' || j.pri_first_name AS name,
                    j.empl_id,
                    pd.dept_gyear,
                    pd.student_gyear,
                    pd.dept_abbr home_dept_abbr,
                    pd.dept_short home_dept_short,
                    j.job_desc,
                    d.discipline_desc AS department,
                    a.appt_start_date,
                    a.planned_appt_end_date,
                    c.subject,
                    c.catalog_nbr AS catalog_number,
                    c.title AS course_title,
                    NVL (a.acad_year, c.sis_acad_year) AS academic_year,
                    NVL (a.term_cd,
                         CASE
                            WHEN c.sis_term_id = '8' THEN 'FALL'
                            WHEN c.sis_term_id = '2' THEN 'SPRING'
                            ELSE TO_CHAR (c.sis_term_id)
                         END)
                       AS term_cd,
                    a.assignment_eff_date,
                    a.assignment_type,
                    a.role_type,
                    a.role_fte,
                    a.section_count,
                    a.status,
                    a.aurora_group,
                    j.job_cd,
                    j.dept_id,
                    j.dept_desc,
                    p.hvrd_email_addr
               FROM aurora_rpt.assg_curr_mv a
                    JOIN HDWDATA.OHR_JOB_ACTN_ALL j
                       ON     j.empl_id = a.empl_id
                          AND j.empl_rcd = a.empl_rcd
                          AND j.eff_date = a.appt_start_date
                          AND j.eff_seq = a.start_job_eff_date_eff_seq
                    JOIN hdwdata.dept d
                       ON d.dept_id = j.dept_id
                    LEFT OUTER JOIN sis_data.AUR_PROGRESS_STU_UNQ pd
                       ON pd.hu_id = a.empl_id
                    LEFT OUTER JOIN SIS_DATA.aur_courses c
                       ON c.cpk = a.cpk_id
                    LEFT OUTER JOIN HDWDATA.ohr_pers_curr p
                       ON p.empl_id = a.empl_id )
                       
                       
   
   
GRANT SELECT ON INST_DATA.GSASBI_AUR_TATF_COURSE_ASSIGN TO GSASBI;

   
   