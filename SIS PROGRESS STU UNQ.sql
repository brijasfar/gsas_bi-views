/*
  CREATE NEW VIEW for Unique Progress Data
  This will source Aurora and GSASBI 
*/



CREATE OR REPLACE VIEW SIS_DATA.PROGRESS_STU_UNQ
AS
    SELECT hu_id, student_gyear, dept_gyear, dept_abbr, dept_short, acad_prog, acad_plan, hu_current_term, generals_met, generals_met_date, english_proficiency, progress, gy_ord
     FROM (  SELECT hu_id, student_gyear, dept_gyear, dept_abbr, dept_short, acad_prog, acad_plan, hu_current_term, generals_met, generals_met_date, english_proficiency, progress, ROW_NUMBER () OVER (PARTITION BY hu_id ORDER BY NVL(student_gyear,0) DESC) as gy_ord
             FROM sis_ods.sis_data_progress_data
             WHERE acad_prog_status = 'AC'  )
    WHERE gy_ord = 1 ;


--CREATE OR REPLACE VIEW SIS_DATA.AUR_PROGRESS_STU_UNQ
CREATE OR REPLACE VIEW SIS_DATA.AUR_PROGRESS_STU_UNQ_2
AS
Select hu_id, student_gyear, dept_gyear, dept_abbr, dept_short
From sis_data.PROGRESS_STU_UNQ  ; 

GRANT SELECT on SIS_DATA.AUR_PROGRESS_STU_UNQ_2 to aurora_rpt


------------------------------------------------------
-- checks


select count(*) from AUR_PROGRESS_STU_UNQ_2
--4344 
select count(*) from AUR_PROGRESS_STU_UNQ
--4344 

select a.hu_id, a.student_gyear as orig_gyear, b.student_gyear as new_gyear
from AUR_PROGRESS_STU_UNQ A
join PROGRESS_STU_UNQ B
on a.hu_id=b.hu_id
where a.student_gyear<>b.student_gyear

------------------------------------------------------------------------------------------------
-- dev 

select distinct progress FROM sis_ods.sis_data_progress_data







